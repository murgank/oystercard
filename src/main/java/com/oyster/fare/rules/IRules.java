package com.oyster.fare.rules;

import com.oyster.entity.Station;
import com.oyster.entity.Transport;

public interface IRules {
  double getFare();
  boolean isApplicable(Station from, Station to, Transport transport);
}
