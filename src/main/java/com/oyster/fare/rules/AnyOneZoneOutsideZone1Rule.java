package com.oyster.fare.rules;

import com.oyster.entity.Station;
import com.oyster.entity.Transport;

public class AnyOneZoneOutsideZone1Rule implements IRules {
  private static AnyOneZoneOutsideZone1Rule instance;

  private AnyOneZoneOutsideZone1Rule() {}

  public static AnyOneZoneOutsideZone1Rule getInstance() {
    if(instance == null) {
      instance = new AnyOneZoneOutsideZone1Rule();
    }
    return instance;
  }

  public double getFare() {
    return 2.0;
  }

  @Override
  public boolean isApplicable(Station from, Station to,
                              Transport transport) {
    return false;
  }
}
