package com.oyster.fare.rules;

import com.oyster.entity.Station;
import com.oyster.entity.Transport;

public class AnyThreeZonesRule implements IRules {
  private static AnyThreeZonesRule instance;

  private AnyThreeZonesRule() {

  }

  public static AnyThreeZonesRule getInstance() {
    if(instance == null) {
      instance = new AnyThreeZonesRule();
    }
    return instance;
  }

  public double getFare() {
    return 3.2;
  }

  @Override
  public boolean isApplicable(Station from, Station to,
                              Transport transport) {
    return false;
  }
}
