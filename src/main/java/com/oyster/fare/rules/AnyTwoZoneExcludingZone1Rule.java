package com.oyster.fare.rules;

import com.oyster.entity.Station;
import com.oyster.entity.Transport;

public class AnyTwoZoneExcludingZone1Rule implements IRules {
  private static AnyTwoZoneExcludingZone1Rule instance;

  private AnyTwoZoneExcludingZone1Rule() {
  }

  public static AnyTwoZoneExcludingZone1Rule getInstance() {
    if(instance == null) {
      instance = new AnyTwoZoneExcludingZone1Rule();
    }
    return instance;
  }

  public double getFare() {
    return 2.25;
  }

  @Override
  public boolean isApplicable(Station from, Station to,
                              Transport transport) {
    return false;
  }
}
