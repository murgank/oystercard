package com.oyster.fare.rules;

import com.oyster.entity.Station;
import com.oyster.entity.Transport;
import com.oyster.entity.Zone;

public class AnyWhereInZone1Rule implements IRules {
  private static AnyWhereInZone1Rule instance;

  private AnyWhereInZone1Rule() {
  }

  public static AnyWhereInZone1Rule getInstance() {
    if (instance == null) {
      instance = new AnyWhereInZone1Rule();
    }
    return instance;
  }

  public double getFare() {
    return 2.5;
  }

  public boolean isApplicable(Station from, Station to,
                              Transport transport) {
    if (Transport.BUS.equals(transport)) {
      return false;
    }
    return from.getZones().contains(Zone.ZONE_1) &&
        to.getZones().contains(Zone.ZONE_1);
  }
}
