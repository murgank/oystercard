package com.oyster.fare.rules;

import com.oyster.entity.Station;
import com.oyster.entity.Transport;

public class AnyTwoZoneIncludingZone1Rule implements IRules {
  private static AnyTwoZoneIncludingZone1Rule instance;

  private AnyTwoZoneIncludingZone1Rule() {
  }

  public static AnyTwoZoneIncludingZone1Rule getInstance() {
    if(instance == null) {
      instance = new AnyTwoZoneIncludingZone1Rule();
    }
    return instance;
  }

  public double getFare() {
    return 3.0;
  }

  @Override
  public boolean isApplicable(Station from, Station to,
                              Transport transport) {
    return false;
  }
}
