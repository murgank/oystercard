package com.oyster.fare.rules;

import com.oyster.entity.Station;
import com.oyster.entity.Transport;

public class AnyBusJourneyRule implements IRules {
  private static AnyBusJourneyRule instance;

  private AnyBusJourneyRule() {
  }

  public static AnyBusJourneyRule getInstance() {
    if (instance == null) {
      instance = new AnyBusJourneyRule();
    }
    return instance;
  }

  public double getFare() {
    return 1.8;
  }

  @Override
  public boolean isApplicable(Station from, Station to,
                              Transport transport) {
    return transport.equals(Transport.BUS);
  }
}
