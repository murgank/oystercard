package com.oyster.fare;

import java.util.Arrays;
import java.util.List;
import com.oyster.entity.Station;
import com.oyster.entity.Transport;
import com.oyster.fare.rules.AnyBusJourneyRule;
import com.oyster.fare.rules.AnyOneZoneOutsideZone1Rule;
import com.oyster.fare.rules.AnyThreeZonesRule;
import com.oyster.fare.rules.AnyTwoZoneExcludingZone1Rule;
import com.oyster.fare.rules.AnyTwoZoneIncludingZone1Rule;
import com.oyster.fare.rules.AnyWhereInZone1Rule;
import com.oyster.fare.rules.IRules;

public class FareCalculator {
  private static double maxFare;

  private static final List<IRules> rules = Arrays.asList(
      AnyWhereInZone1Rule.getInstance(),
      AnyBusJourneyRule.getInstance(),
      AnyOneZoneOutsideZone1Rule.getInstance(),
      AnyThreeZonesRule.getInstance(),
      AnyTwoZoneExcludingZone1Rule.getInstance(),
      AnyTwoZoneIncludingZone1Rule.getInstance()
  );

  private FareCalculator() {
  }

  public static double getMaxFare() {
    if(maxFare != 0) {
      return maxFare;
    }
    for (IRules rule : rules) {
      if (maxFare < rule.getFare()) {
        maxFare = rule.getFare();
      }
    }
    return maxFare;
  }

  public static double getCreditAmount(Station from, Station to,
                                       Transport transport) {
    double minFare = getMaxFare();
    for (IRules rule : rules) {
      if (rule.isApplicable(from,to,transport) && minFare > rule.getFare()) {
        minFare = rule.getFare();
      }
    }
    return maxFare - minFare;
  }
}
