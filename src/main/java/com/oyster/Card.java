package com.oyster;

import com.oyster.entity.Station;
import com.oyster.entity.Transport;
import com.oyster.exception.InsufficientBalanceException;
import com.oyster.fare.FareCalculator;

public class Card {
  private double balance;
  private Station fromStation;
  private Transport transport;

  public Card(double balance) {
    this.balance = balance;
  }

  public double getBalance() {
    return balance;
  }

  public void debit(double amount) throws InsufficientBalanceException {
    if(this.balance < amount) {
      throw new InsufficientBalanceException();
    }
    this.balance -= amount;
  }

  public void credit(double amount) {
    this.balance += amount;
  }

  public void startTrip(Station from, Transport transport) {
    if(from == null || transport == null) return;
    double maxFare = FareCalculator.getMaxFare();
    try {
      debit(maxFare);
      fromStation = from;
      this.transport = transport;
    } catch (InsufficientBalanceException ex) {
      System.out.println("Insufficient Balance Cannot start trip");
    }
  }

  public void endTrip(Station toStation) {
    if(fromStation == null || transport == null || toStation == null) return;
    double creditFare = FareCalculator.getCreditAmount(fromStation,toStation,transport);
    credit(creditFare);
    fromStation = null;
    transport = null;
  }
}
