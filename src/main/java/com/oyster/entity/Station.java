package com.oyster.entity;

import java.util.List;

public class Station {
  private String name;
  private List<Zone> zones;

  public Station(String name,List<Zone> zones) {
    this.name = name;
    this.zones = zones;
  }

  public List<Zone> getZones() {
    return zones;
  }
}
