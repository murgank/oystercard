package com.oyster;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import static org.junit.jupiter.api.Assertions.*;
import com.oyster.entity.Station;
import com.oyster.entity.Transport;
import com.oyster.entity.Zone;
import com.oyster.exception.InsufficientBalanceException;
import org.junit.jupiter.api.Test;

class CardTest {

  @Test
  void shouldCreateCardWithBalance() {
    Card card = new Card(2.0);

    assertEquals(2.0,card.getBalance());
  }

  @Test
  void shouldDebitCard() throws InsufficientBalanceException {
    Card card = new Card(2.0);

    card.debit(0.1);

    assertEquals(1.9,card.getBalance());
  }

  @Test
  void shouldThrowErrorIFAmountIsLessThanDebitAmount() {
    Card card = new Card(2.0);

    assertThrows(InsufficientBalanceException.class,() -> card.debit(2.1));
  }

  @Test
  void shouldCreditAmountIntoCard() {
    Card card = new Card(0.0);

    card.credit(2.0);

    assertEquals(2.0,card.getBalance());
  }

  @Test
  void shouldDebitCardWithMaxFareWhenTripStarts() {
    Station startStation = new Station("Holborn",new ArrayList<Zone>() {{
      add(Zone.ZONE_1);
    }});
    Card card = new Card(30);
    card.startTrip(startStation,Transport.TUBE);

    assertEquals(26.8,card.getBalance());
  }

  @Test
  void shouldNotDebitCardIfInsufficientAmount() {
    Station startStation = new Station("Holborn",new ArrayList<Zone>() {{
      add(Zone.ZONE_1);
    }});
    Card card = new Card(1);
    card.startTrip(startStation,Transport.TUBE);

    assertEquals(1.0,card.getBalance());
  }

  @Test
  void shouldChargeLeastPossibleFareCharge() {
    List<Zone> zones = Collections.singletonList(Zone.ZONE_1);
    Station from = new Station("start",zones);
    Station to = new Station("end",zones);
    Card card = new Card(30);

    card.startTrip(from,Transport.BUS);
    card.endTrip(to);

    assertEquals(28.2,card.getBalance());
  }

  @Test
  void shouldNotCreditTwiceLeastPossibleFare() {
    List<Zone> zones = Collections.singletonList(Zone.ZONE_1);
    Station from = new Station("start",zones);
    Station to = new Station("end",zones);
    Card card = new Card(30);

    card.startTrip(from,Transport.BUS);
    card.endTrip(to);
    card.endTrip(to);

    assertEquals(28.2,card.getBalance());
  }

  @Test
  void shouldNotStartTripIfFromStationAndTransportIsNotAvailable() {
    Card card = new Card(30);

    card.startTrip(null,Transport.BUS);

    assertEquals(30.0,card.getBalance());

    List<Zone> zones = Collections.singletonList(Zone.ZONE_1);
    Station from = new Station("start",zones);
    card.startTrip(from, null);

    assertEquals(30.0,card.getBalance());
  }

  @Test
  void shouldNotEndTripIfTripIsNotStarted() {
    Card card = new Card(30);
    List<Zone> zones = Collections.singletonList(Zone.ZONE_1);
    Station endStation = new Station("endStation",zones);

    card.endTrip(endStation);

    assertEquals(30.0,card.getBalance());
  }

  @Test
  void shouldNotEndTripIfEndStationIsNotAvailable() {
    List<Zone> zones = Collections.singletonList(Zone.ZONE_1);
    Station from = new Station("start",zones);
    Card card = new Card(30);

    card.startTrip(from,Transport.BUS);
    card.endTrip(null);

    assertEquals(26.8,card.getBalance());
  }
}
