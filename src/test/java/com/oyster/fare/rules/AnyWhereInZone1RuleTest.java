package com.oyster.fare.rules;

import java.util.Collections;
import java.util.List;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import com.oyster.entity.Station;
import com.oyster.entity.Transport;
import com.oyster.entity.Zone;
import org.junit.jupiter.api.Test;

class AnyWhereInZone1RuleTest {
  @Test
  void shouldReturnCorrectFare() {
    AnyWhereInZone1Rule rule = AnyWhereInZone1Rule.getInstance();

    assertEquals(2.5,rule.getFare());
  }

  @Test
  void shouldHaveOnlyOneInstance() {
    AnyWhereInZone1Rule rule1 = AnyWhereInZone1Rule.getInstance();
    AnyWhereInZone1Rule rule2 = AnyWhereInZone1Rule.getInstance();

    assertEquals(rule1,rule2);
  }

  @Test
  void shouldNotBeApplicableIfTransportIsBus() {
    AnyWhereInZone1Rule rule = AnyWhereInZone1Rule.getInstance();
    List<Zone> zones = Collections.singletonList(Zone.ZONE_1);
    Station from = new Station("start",zones);
    Station to = new Station("end",zones);
    Transport transport = Transport.BUS;

    boolean expected = rule.isApplicable(from,to,transport);

    assertFalse(expected);
  }

  @Test
  void shouldBeApplicableIfFromStationAndTwoStationAreInZone1() {
    AnyWhereInZone1Rule rule = AnyWhereInZone1Rule.getInstance();
    List<Zone> zones = Collections.singletonList(Zone.ZONE_1);
    Station from = new Station("start",zones);
    Station to = new Station("end",zones);
    Transport transport = Transport.TUBE;

    boolean expected = rule.isApplicable(from,to,transport);

    assertTrue(expected);
  }

  @Test
  void shouldNotBeApplicableIfAnyStationIsNotInZone1() {
    AnyWhereInZone1Rule rule = AnyWhereInZone1Rule.getInstance();
    Station from = new Station("start",Collections.singletonList(Zone.ZONE_1));
    Station to = new Station("end",Collections.singletonList(Zone.ZONE_2));
    Transport transport = Transport.TUBE;

    boolean expected = rule.isApplicable(from,to,transport);

    assertFalse(expected);
  }
}
