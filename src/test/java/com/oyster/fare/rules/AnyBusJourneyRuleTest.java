package com.oyster.fare.rules;

import java.util.Collections;
import java.util.List;
import static org.junit.jupiter.api.Assertions.*;
import com.oyster.entity.Station;
import com.oyster.entity.Transport;
import com.oyster.entity.Zone;
import org.junit.jupiter.api.Test;

class AnyBusJourneyRuleTest {
  @Test
  void shouldReturnCorrectFare() {
    AnyBusJourneyRule rule = AnyBusJourneyRule.getInstance();

    assertEquals(1.8,rule.getFare());
  }

  @Test
  void shouldHaveOnlyOneInstance() {
    AnyBusJourneyRule rule1 = AnyBusJourneyRule.getInstance();
    AnyBusJourneyRule rule2 = AnyBusJourneyRule.getInstance();

    assertEquals(rule1,rule2);
  }

  @Test
  void shouldBeApplicableIfTransportIsBus() {
    AnyBusJourneyRule rule = AnyBusJourneyRule.getInstance();
    List<Zone> zones = Collections.singletonList(Zone.ZONE_1);
    Station from = new Station("start",zones);
    Station to = new Station("end",zones);
    Transport transport = Transport.BUS;

    boolean expected = rule.isApplicable(from,to,transport);

    assertTrue(expected);
  }

  @Test
  void shouldNotBeApplicableIfTransportIsTube() {
    AnyBusJourneyRule rule = AnyBusJourneyRule.getInstance();
    List<Zone> zones = Collections.singletonList(Zone.ZONE_1);
    Station from = new Station("start",zones);
    Station to = new Station("end",zones);
    Transport transport = Transport.TUBE;

    boolean expected = rule.isApplicable(from,to,transport);

    assertFalse(expected);
  }
}
