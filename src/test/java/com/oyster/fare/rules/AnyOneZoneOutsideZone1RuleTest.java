package com.oyster.fare.rules;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;

class AnyOneZoneOutsideZone1RuleTest {
  @Test
  void shouldReturnCorrectFare() {
    AnyOneZoneOutsideZone1Rule rule = AnyOneZoneOutsideZone1Rule.getInstance();

    assertEquals(2.0,rule.getFare());
  }

  @Test
  void shouldHaveOnlyOneInstance() {
    AnyOneZoneOutsideZone1Rule rule1 = AnyOneZoneOutsideZone1Rule.getInstance();
    AnyOneZoneOutsideZone1Rule rule2 = AnyOneZoneOutsideZone1Rule.getInstance();

    assertEquals(rule1,rule2);
  }
}
