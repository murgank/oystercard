package com.oyster.fare.rules;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

class AnyThreeZonesRuleTest {
  @Test
  void shouldReturnCorrectFare() {
    AnyThreeZonesRule rule = AnyThreeZonesRule.getInstance();

    assertEquals(3.2,rule.getFare());
  }

  @Test
  void shouldHaveOnlyOneInstance() {
    AnyThreeZonesRule rule1 = AnyThreeZonesRule.getInstance();
    AnyThreeZonesRule rule2 = AnyThreeZonesRule.getInstance();

    assertEquals(rule1,rule2);
  }
}
