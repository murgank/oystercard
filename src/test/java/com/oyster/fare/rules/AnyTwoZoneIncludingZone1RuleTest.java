package com.oyster.fare.rules;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

class AnyTwoZoneIncludingZone1RuleTest {
  @Test
  void shouldReturnCorrectFare() {
    AnyTwoZoneIncludingZone1Rule rule = AnyTwoZoneIncludingZone1Rule.getInstance();

    assertEquals(3.0,rule.getFare());
  }

  @Test
  void shouldHaveOnlyOneInstance() {
    AnyTwoZoneIncludingZone1Rule rule1 = AnyTwoZoneIncludingZone1Rule.getInstance();
    AnyTwoZoneIncludingZone1Rule rule2 = AnyTwoZoneIncludingZone1Rule.getInstance();

    assertEquals(rule1,rule2);
  }
}
