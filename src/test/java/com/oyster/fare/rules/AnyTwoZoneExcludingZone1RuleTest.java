package com.oyster.fare.rules;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

class AnyTwoZoneExcludingZone1RuleTest {
  @Test
  void shouldReturnCorrectFare() {
    AnyTwoZoneExcludingZone1Rule rule = AnyTwoZoneExcludingZone1Rule.getInstance();

    assertEquals(2.25,rule.getFare());
  }

  @Test
  void shouldHaveOnlyOneInstance() {
    AnyTwoZoneExcludingZone1Rule rule1 = AnyTwoZoneExcludingZone1Rule.getInstance();
    AnyTwoZoneExcludingZone1Rule rule2 = AnyTwoZoneExcludingZone1Rule.getInstance();

    assertEquals(rule1,rule2);
  }
}
