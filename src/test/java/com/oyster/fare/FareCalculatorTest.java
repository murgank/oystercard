package com.oyster.fare;

import java.util.Collections;
import java.util.List;
import static org.junit.jupiter.api.Assertions.assertEquals;
import com.oyster.entity.Station;
import com.oyster.entity.Transport;
import com.oyster.entity.Zone;
import org.junit.jupiter.api.Test;

class FareCalculatorTest {
  @Test
  void shouldGetMaxFareFromAllRules() {
    double maxFare = FareCalculator.getMaxFare();

    assertEquals(3.2,maxFare);
  }

  @Test
  void shouldGetCreditAmountFromAllApplicableFareRules() {
    List<Zone> zones = Collections.singletonList(Zone.ZONE_1);
    Station from = new Station("start",zones);
    Station to = new Station("end",zones);
    Transport transport = Transport.BUS;

    double creditAmount = FareCalculator.getCreditAmount(from,to,transport);

    assertEquals((3.2-1.8),creditAmount);
  }
}
