package com.oyster.entity;

import java.util.ArrayList;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

class StationTest {

  @Test
  void shouldCreateStationWithNameAndZone() {
    ArrayList<Zone> zones = new ArrayList<>();
    zones.add(Zone.ZONE_1);
    Station someStation = new Station("Station Name",zones);

    assertEquals(zones,someStation.getZones());
  }
}
